import org.omg.CORBA.PUBLIC_MEMBER;

import javax.swing.*;

public class Table {

	private Player x;
	private Player o;
	private Player currentPlayer;
	private int turnCount;
	private Player winner;
	private char[][] table = {
			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'}
	};

	public Table(Player x,Player o) {
		this.x=x;
		this.o=o;
		currentPlayer=x;
		winner = null;
	}

	public char[][] getTable(){
		return table;
	}

	public Player getCurrentPlayer(){
		return currentPlayer;
	}

	public boolean setTable(int row,int col){
		if(table[row][col]=='-'){
			table[row][col]=currentPlayer.getName();
			return true;
		}
		return false;
	}

	private boolean checkWinX(){
		if(table[0][0]==('X')&&table[0][1]==('X')&&table[0][2]==('X')) {
			return true;
		}else if(table[1][0]==('X')&&table[1][1]==('X')&&table[1][2]==('X')) {
			return true;
		}else if(table[2][0]==('X')&&table[2][1]==('X')&&table[2][2]==('X')) {
			return true;
		}else if(table[0][0]==('X')&&table[1][0]==('X')&&table[2][0]==('X')) {
			return true;
		}else if(table[0][1]==('X')&&table[1][1]==('X')&&table[2][1]==('X')) {
			return true;
		}else if(table[0][2]==('X')&&table[1][2]==('X')&&table[2][2]==('X')) {
			return true;
		}else if(table[0][0]==('X')&&table[1][1]==('X')&&table[2][2]==('X')) {
			return true;
		}else if(table[0][2]==('X')&&table[1][1]==('X')&&table[2][0]==('X')) {
			return true;
		}

		return false;
	}

	private boolean checkWinO(){
		if(table[0][0]==('O')&&table[0][1]==('O')&&table[0][2]==('O')) {
			return true;
		}else if(table[1][0]==('O')&&table[1][1]==('O')&&table[1][2]==('O')) {
			return true;
		}else if(table[2][0]==('O')&&table[2][1]==('O')&&table[2][2]==('O')) {
			return true;
		}else if(table[0][0]==('O')&&table[1][0]==('O')&&table[2][0]==('O')) {
			return true;
		}else if(table[0][1]==('O')&&table[1][1]==('O')&&table[2][1]==('O')) {
			return true;
		}else if(table[0][2]==('O')&&table[1][2]==('O')&&table[2][2]==('O')) {
			return true;
		}else if(table[0][0]==('O')&&table[1][1]==('O')&&table[2][2]==('O')) {
			return true;
		}else if(table[0][2]==('O')&&table[1][1]==('O')&&table[2][0]==('O')) {
			return true;
		}
		return false;
	}

	public boolean checkDraw(){
		if (turnCount==9){
			x.drow();
			o.drow();
			return true;
		}
		return false;
	}

	public boolean checkWin(){
		if(checkWinO()||checkWinX()){
			winner = currentPlayer;
			if (currentPlayer==x){
				x.win();
				o.lose();
			}else{
				o.win();
				x.lose();
			}
			return true;
		}
		return false;
	}


	public boolean isFinish(){
		if(checkWin()){

			return true;
		}
		if(checkDraw()){
			return true;
		}
		return false;
	}

	public void switchPlayer(){
		if(currentPlayer==x){
			currentPlayer=o;
		}else{
			currentPlayer=x;
		}
		turnCount++;
	}

	public Player getWinner(){
		return winner;
	}
}
