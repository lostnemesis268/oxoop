import java.util.Scanner;

public class Game {
	private Player x;
	private Player o;
	private Table table;
	
	public Game() {
		x = new Player('X');
		o = new Player('O');

	}
	
	public void play() {

		for (;;){
			table = new Table(x,o);
			playOne();
		}

	}

	public void playOne(){
		printWelcome();
		for(;;){
			printTable();
			printTurn();
			input();
			if (table.isFinish()){
				break;
			}
			table.switchPlayer();
		}
		printTable();
		showWinner();
		showStat();
	}


	private void showStat() {
		System.out.println(x.getName()+" :W,D,L"
				+ x.getWin()+","
				+ x.getDrow()+","
				+ x.getLose());
		System.out.println(o.getName()+" :W,D,L:>"
				+ o.getWin()+","
				+ o.getDrow()+","
				+ o.getLose());
	}

	private void showWinner(){
		Player player = table.getWinner();
		System.out.println(player.getName() + " win");
	}

	private void printWelcome() {
		System.out.println("Welcome to OX Game");
	}

	private void printTable(){
		char[][] board = table.getTable();
		System.out.println("  1 2 3");
		for (int i=0;i<board.length;i++){
			System.out.print(i+1);
			for (int j=0;j<board[i].length;j++){
				System.out.print(" "+board[i][j]);
			}
			System.out.println();
		}
	}

	private void printTurn(){
		Player player = table.getCurrentPlayer();
		System.out.println(player.getName()+" turn");
	}

	private void input(){
		Scanner scan = new Scanner(System.in);

		System.out.println("Please input Row,Col:");
		int r,c;
		String row=scan.next();
		String col=scan.next();
		for (;;){


			if(row.equals("1")) {
				r=0;
			}else if(row.equals("2")){
				r=1;
			}else if(row.equals("3")){
				r=2;
			}else{
				System.out.println("Please enter Row number 1-3.");
				row=scan.next();
				continue;
			}


			if(col.equals("1")) {
				c=0;
			}else if(col.equals("2")) {
				c=1;
			}else if(col.equals("3")) {
				c=2;
			}else {
				System.out.println("Please enter Col number 1-3.");
				col=scan.next();
				continue;
			}


			if (table.setTable(r,c)==false){
				System.out.println("That position is already used.");
				System.out.println("Please input Row,Col:");
				row=scan.next();
				col=scan.next();
				continue;
			}
			break;
		}


	}

}
