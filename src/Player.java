
public class Player {
	
	private char name;
	private int win,drow,lose;
	
	public Player(char name) {
		this.name=name;
	}

	public char getName(){
		return name;
	}

	public void win(){
		win++;
	}
	public void drow(){
		drow++;
	}
	public void lose(){
		lose++;
	}

	public int getWin() {
		return win;
	}

	public int getDrow() {
		return drow;
	}

	public int getLose() {
		return lose;
	}
}
